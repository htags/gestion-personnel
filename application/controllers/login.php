<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 28/01/2019
 * Time: 09:03
 */


class login extends CI_Controller

{
    public function  __construct()
    {
        parent::__construct();
        $this->load->model('Home_model', 'home', TRUE);
        $this->load->helper('url_helper', 'email');
        $this->load->library('form_validation', array('session'));
        $this->load->library(array('session'));

    }
    public function index()
    {
        $this->load->view('login');
    }


}
